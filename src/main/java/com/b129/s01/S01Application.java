package com.b129.s01;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class S01Application {

	public static void main(String[] args) {
		SpringApplication.run(S01Application.class, args);
	}

	// http://localhost:8080/api/hello
	@GetMapping("api/hello")
	public String hello() {
		return "Hello, world!";
	}

	// http://localhost:8080/api/hello-friend?name=nameInput
	@GetMapping("api/hello-friend")
	public String helloFriend(@RequestParam(value = "name", defaultValue = "world") String name) {
		return String.format("Hello, %s!", name);
	}

	// http://localhost:8080/api/who-are-you?name=nameInput&age=ageInput
	@GetMapping("api/who-are-you")
	public String whoAreYou(@RequestParam(value = "name") String name, @RequestParam(value = "age") String age) {
		return String.format("Hello, I am %s! I am %s years old.", name, age);
	}

	// http://localhost:8080/api/my-name-is/name
	@GetMapping("api/my-name-is/{name}")
	public String myNameIs(@PathVariable("name") String name) {
		return String.format("My name is %s!", name);
	}

}
